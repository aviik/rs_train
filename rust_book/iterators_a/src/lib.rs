#[derive(Debug, Clone)]
struct Book {
    name: String,
    authors: Vec<String>,
}

impl Book {
    fn new(name: String, authors: Vec<String>) -> Self {
        Self { name, authors }
    }
}

#[derive(Debug, Clone)]
struct BookLibrary {
    books: Vec<Book>,
    count: usize,
}

impl BookLibrary {
    fn new() -> Self {
        Self {
            books: Vec::new(),
            count: 0,
        }
    }

    fn add_book(&mut self, book: Book) {
        self.books.push(book);
    }
}

impl Iterator for BookLibrary {
    type Item = Book;

    fn next(&mut self) -> Option<Self::Item> {
        match self.books.get(self.count) {
            Some(s) => {
                self.count += 1;
                Some(s.to_owned())
            }
            None => None,
        }
    }
}

#[test]
fn libray_ds_test() {
    let mut prog_book_library = BookLibrary::new();
    // let book_a: Book = Book::new("The C Programming Language".to_string(),
    //                              vec!["Brian Kerrighan".to_string(),"Dennis Ritchie".to_string()]);
    prog_book_library.add_book(Book::new(
        "The C Programming Language".to_string(),
        vec!["Brian Kerrighan".to_string(), "Dennis Ritchie".to_string()],
    ));

    prog_book_library.add_book(Book::new(
        "Head First Python".to_string(),
        vec!["Paul Barry".to_string()],
    ));

    println!("{:?}", prog_book_library.next());
    println!("{:?}", prog_book_library.next());
    println!("{:?}", prog_book_library.next());
}
